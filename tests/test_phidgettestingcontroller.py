import sys
import os
import time
from decimal import *

# python 3 and 2 compatibility import and global variables
if (sys.version_info > (3, 0) or sys.version < (2, 7)):
    # Python 3 imports  in this block
    import io as StringIO
else:
    # Python 2 imports in this block
    import StringIO

if sys.version_info < (2, 7):
    import unittest2 as unittest
else:
    import unittest

from controller import *

from Phidgets import Manager
from Phidgets.Devices.InterfaceKit import InterfaceKit

# python 3 and 2 compatibility import and global variables
if (sys.version_info > (3, 0)):
    # Python 3 imports  in this block
    from configparser import *

    SafeConfigParser = ConfigParser
    pythonversion = 3
else:
    # Python 2 imports in this block
    from ConfigParser import *

# set up file references
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
os.path.join(THIS_DIR, 'testdata.csv')
test_script = os.path.join(THIS_DIR, 'shorttest.script')
config_file = os.path.join(THIS_DIR, 'test.config')

# set up logger to test logs
logfile = os.path.join(THIS_DIR, 'testlogger.config')
configure_logging(date=True, log_config=logfile)

class TestConvertSeconds(unittest.TestCase):
    def test_convertSeconds_postive(self):
        self.assertEqual(convertSeconds(41901), (11, 38, 21))

    def test_convertSeconds_negative(self):
        self.assertEqual(convertSeconds(-41901), (-11, 38, 21))

    def test_convertSeconds_decimal(self):
        self.assertEqual(convertSeconds(Decimal('41901')), (11, 38, 21))


class TestGetPhidgetManager(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_phidget_manager_local(self):
        kit_list, mngr = get_phidget_manager()
        self.assertTrue(isinstance(kit_list, list))
        self.assertTrue(isinstance(mngr, Manager.Manager))
        mngr.closeManager()

    def test_get_phidget_manager_remote(self):
        kit_list, mngr = get_phidget_manager("172.22.2.160", 5001)
        self.assertTrue(isinstance(kit_list, list))
        self.assertTrue(isinstance(mngr, Manager.Manager))
        mngr.closeManager()

    # def test_get_phidget_manager_no_connection(self):
    #     with self.assertRaises(PhidgetException):
    #         get_phidget_manager("172.22.2.160", 5000)

    def tearDown(self):
        pass


class TestLoadScript(unittest.TestCase):
    def setUp(self):
        pass

    def test_load_script(self):
        cycles_expected = 2
        total_time_expected = 4.0
        timing_data_expected = [[1.0, 1.0] for x in range(2)]
        testscript = load_script(test_script)
        time_expected = int(time.time())
        self.assertIsInstance(testscript, TimingData)
        self.assertAlmostEqual(testscript.starttime, time_expected,
                               msg="Failed time test {0} ~!= {1}".format(time_expected, testscript.starttime))
        self.assertEqual(testscript.cycles, cycles_expected,
                         msg='Cycles do not match {0} != {1}'.format(cycles_expected, testscript.cycles))
        self.assertEqual(testscript.timingdata, timing_data_expected,
                         msg='Timing DataDoes not match\n {0}\n != \n {1}'.format(testscript.timingdata,
                                                                                  timing_data_expected))
        self.assertEqual(testscript.totaltime, total_time_expected,
                         msg='Times do not match {0} != {1}'.format(testscript.totaltime, total_time_expected))

    def tearDown(self):
        pass


class TestCalcCycles(unittest.TestCase):
    def setUp(self):
        self.testdata_compare = []
        for i in range(5):
            self.testdata_compare.append([10, 5])
        self.obj = calc_cycles(5, 10, 5)

    def test_load_script(self):
        self.assertAlmostEqual(int(time.time()), self.obj.starttime, msg="Failed time test")
        self.assertEqual(5, self.obj.cycles, msg='Cycles do not match')
        self.assertEqual(self.obj.timingdata, self.testdata_compare, msg='Timing DataDoes not match')
        self.assertEqual(self.obj.totaltime, 75, msg='Times do not match {0} != 75'.format(self.obj.totaltime))

    def tearDown(self):
        pass


class TestRunCyclesRemote(unittest.TestCase):
    def setUp(self):
        self.devicelist, self.mngr = get_phidget_manager("172.22.2.160", 5001)
        if not self.devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestRunSingleRemote: No Devices Attached to test")
        config = getConfig(config_file)
        config_device_name = 'testremote'
        config_devices = [[config_device_name, config.getint(config_device_name, 'ikitserial')]]

        self.devices, self.ifaces = initialise_devices(config_devices, config, "172.22.2.160", 5001, False)

    def test_run_cycles_script(self):
        timingobj = load_script(test_script)
        self.assertTrue(run_cycles(self.devices, timingobj, True, True))

    def test_run_cycles_cycles(self):
        timingobj = calc_cycles(2, 1, 1)
        self.assertTrue(run_cycles(self.devices, timingobj, True, True))

    def tearDown(self):
        self.mngr.closeManager()
        for kit, serial in self.ifaces:
            close_InterfaceKit(kit)


class TestRunCyclesLocal(unittest.TestCase):
    def setUp(self):
        self.devicelist, self.mngr = get_phidget_manager()
        if not self.devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestRunCyclesLocal: No Devices Attached to test")
        config = getConfig(config_file)
        config_device_name = 'testremote'
        config_devices = [[config_device_name, config.getint(config_device_name, 'ikitserial')]]
        self.devices, self.ifaces = initialise_devices(config_devices, config, False, False, False)

    def test_run_cycles_script(self):
        timingobj = load_script(test_script)
        self.assertTrue(run_cycles(self.devices, timingobj, True, True))

    def test_run_cycles_cycles(self):
        timingobj = calc_cycles(2, 1, 1)
        self.assertTrue(run_cycles(self.devices, timingobj, True, True))

    def tearDown(self):
        self.mngr.closeManager()
        for kit, serial in self.ifaces:
            close_InterfaceKit(kit)


class TestRunSingleRemote(unittest.TestCase):
    def setUp(self):
        devicelist, self.mngr = get_phidget_manager("172.22.2.160", 5001)
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestRunSingleRemote: No Devices Attached to test")
        config = getConfig(config_file)
        config_device_name = 'testremote'
        config_devices = [[config_device_name, config.getint(config_device_name, 'ikitserial')]]

        self.devices, self.ifaces = initialise_devices(config_devices, config, "172.22.2.160", 5001, False)

    def test_run_single(self):
        self.assertTrue(run_single(self.devices, True, True, 'on'))

    def tearDown(self):
        self.mngr.closeManager()
        for kit, serial in self.ifaces:
            close_InterfaceKit(kit)


class TestRunSingleLocal(unittest.TestCase):
    def setUp(self):
        devicelist, self.mngr = get_phidget_manager()
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestRunSingleLocal: No Devices Attached to test")
        config = getConfig(config_file)
        config_device_name = 'testremote'
        config_devices = [[config_device_name, config.getint(config_device_name, 'ikitserial')]]
        self.devices, self.ifaces = initialise_devices(config_devices, config, False, False, False)

    def test_run_single(self):
        self.assertTrue(run_single(self.devices, True, True, 'on'))

    def tearDown(self):
        self.mngr.closeManager()
        for kit, serial in self.ifaces:
            close_InterfaceKit(kit)


class TestInitialiseDevicesLocal(unittest.TestCase):
    def setUp(self):
        devicelist, self.mngr = get_phidget_manager()
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestInitialiseDevicesLocal: No Devices Attached to test")
        config = getConfig(config_file)
        config_device_name = 'testlocal'
        self.config = getConfig(config_file)
        self.device = [[config_device_name, config.getint(config_device_name, 'ikitserial')]]

    def test_initialise_devices(self):
        device_list, ifaces = initialise_devices(self.device, self.config, False, False, False)
        self.assertIsInstance(device_list[0], Device)
        self.assertIsInstance(ifaces[0], InterfaceKit)
        for kit in ifaces:
            close_InterfaceKit(kit)

    def tearDown(self):
        self.mngr.closeManager()


class TestInitialiseDevicesRemote(unittest.TestCase):
    def setUp(self):
        devicelist, self.mngr = get_phidget_manager("172.22.2.160", 5001)
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestInitialiseDevicesRemote: No Devices Attached to test")

        self.config = getConfig(config_file)
        self.device_name = 'testremote'
        self.device = [[self.device_name, self.config.getint(self.device_name, 'ikitserial')]]

    def test_initialise_devices(self):
        device_list, ifaces = initialise_devices(self.device, self.config, "172.22.2.160", 5001, False)
        self.assertIsInstance(device_list[0], Device)
        self.assertIsInstance(ifaces[0][0], InterfaceKit)
        for kit, serial in ifaces:
            close_InterfaceKit(kit)

    def tearDown(self):
        self.mngr.closeManager()


class TestOpen_InterfaceKitLocal(unittest.TestCase):
    def setUp(self):
        devicelist, self.mngr = get_phidget_manager()
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestOpen_InterfaceKitLocal: No Devices Attached to test")
        self.ikit = open_InterfaceKit()

    def test_open_InterfaceKit(self):
        self.assertTrue(isinstance(self.ikit, InterfaceKit))

    def tearDown(self):
        self.mngr.closeManager()
        self.ikit.closePhidget()


class TestOpen_InterfaceKitRemote(unittest.TestCase):
    def setUp(self):
        devicelist, self.mngr = get_phidget_manager("172.22.2.160", 5001)
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestOpen_InterfaceKitRemote: No Devices Attached to test")
        self.ikit = open_InterfaceKit(391811, "172.22.2.160", 5001)

    def test_open_InterfaceKit(self):
        self.assertTrue(isinstance(self.ikit, InterfaceKit))

    def tearDown(self):
        self.mngr.closeManager()
        self.ikit.closePhidget()


class TestControllerLocal(unittest.TestCase):
    def setUp(self):
        config = getConfig(config_file)
        device = 'testlocal'
        device_serial = config.getint(device, 'ikitserial')
        self.ifaces = []
        devicelist, self.mngr = get_phidget_manager()
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestControllerRemote: No Devices Attached to test")

        for ikit, serial in devicelist:
            if device_serial == serial:
                self.ifaces.append([open_InterfaceKit(serial), serial])

        for iface, serial in self.ifaces:
            if serial == device_serial:
                self.controller = Device(device, config, iface)

    def test_changeOutput(self):
        self.assertTrue(self.controller.changeOutput(True, True, 'on'))

    def test_getState(self):
        self.assertEqual(self.controller.getState(True), (True, True))

    def tearDown(self):
        self.mngr.closeManager()
        for kit, serial in self.ifaces:
            kit.closePhidget()


class TestControllerRemote(unittest.TestCase):
    def setUp(self):
        config = getConfig(config_file)
        device = 'testremote'
        device_serial = config.getint(device, 'ikitserial')
        self.ifaces = []
        devicelist, self.mngr = get_phidget_manager("172.22.2.160", 5001)
        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping TestControllerRemote: No Devices Attached to test")

        for ikit, serial in devicelist:
            if device_serial == serial:
                self.ifaces.append([open_InterfaceKit(serial, b"172.22.2.160", 5001), serial])

        for iface, serial in self.ifaces:
            if serial == device_serial:
                self.controller = Device(device, config, iface)

    def test_changeOutput(self):
        self.assertTrue(self.controller.changeOutput(True, True, 'on'))

    def test_getState(self):
        self.assertEqual(self.controller.getState(True), (True, True))

    def tearDown(self):
        self.mngr.closeManager()
        for kit, serial in self.ifaces:
            kit.closePhidget()


class TestGetConfig(unittest.TestCase):
    def test_getConfig(self):
        testfile = 'noexist.cfg'
        self.assertTrue(isinstance(getConfig(testfile), SafeConfigParser), msg="Failed testing a none existent config")
        os.remove(testfile)
        self.assertTrue(isinstance(getConfig(config_file), SafeConfigParser), msg="Failed testing existing config")


if __name__ == '__main__':
    unittest.main()
